// email-validator.js
const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

function validate(email) {
  const emailEnding = email.split('@')[1];
  return VALID_EMAIL_ENDINGS.includes(emailEnding);
}

export { validate };

