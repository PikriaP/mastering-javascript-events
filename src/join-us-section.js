const joinUsSection = (function () {
  
    function createStandardProgram() {
      const section = document.createElement("section");
      section.setAttribute("id", "joinProgramSection");
      section.classList.add("join-us");
      
  
      const heading = document.createElement("h2");
      heading.textContent = "Join Our Program";
      heading.classList.add("join-us-h2");
  
      const paragraph = document.createElement("p");
      paragraph.textContent =
        "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
      paragraph.classList.add("join-us-p");
  
      const form = document.createElement("form");
  
      const emailInput = document.createElement("input");
      emailInput.setAttribute("type", "email");
      emailInput.setAttribute("placeholder", "Email");
      emailInput.classList.add("join-us-form");
  
  
      const subscribeButton = document.createElement("button");
      subscribeButton.textContent = "Subscribe";
      subscribeButton.classList.add("join-us-button");
  
      form.appendChild(emailInput);
      form.appendChild(subscribeButton);
  
      section.appendChild(heading);
      section.appendChild(paragraph);
      section.appendChild(form);
  
      form.addEventListener("submit", function (event) {
        event.preventDefault();
  
        let emailValue = emailInput.value;
  
        console.log("Entered email:", emailValue);
      });
  
      let footer = document.querySelector('footer');

      footer.parentNode.insertBefore(section, footer);
  }
  
    function removeSection() {
      const section = document.getElementById("joinProgramSection");
      if (section) {
        section.parentNode.removeChild(section);
      }
    }
  
    return {
      createStandardProgram,
      removeSection,
    };
  })();

const SectionCreator = {
    create(type) {
      if (type === "standard") {
        return joinUsSection.createStandardProgram();
      } else if (type === "advanced") {
        const section = joinUsSection.createStandardProgram();
        const heading = section.querySelector("h2");
        const button = section.querySelector("button");
        heading.textContent = "Join Our Advanced Program";
        button.textContent = "Subscribe to Advanced Program";
        return section;
      } else {
        throw new Error("Invalid section type.");
      }
    },
  };
  
  export { SectionCreator };
    
