// main.js
import { SectionCreator } from "./join-us-section.js";
import { validate } from "./email-validator.js";

document.addEventListener("DOMContentLoaded", () => {
  const appContainer = document.getElementById("app-container");
  const standardSection = SectionCreator.create("standard");
  appContainer.appendChild(standardSection);

  // Move the event listener code here, after the form is created
  const form = document.getElementById('email-form');
  const emailInput = document.getElementById('email-input');
  const subscribeButton = document.getElementById('subscribe-button');
  const unsubscribeButton = document.getElementById('unsubscribe-button');

  form.addEventListener('submit', (event) => {
    event.preventDefault();
    const email = emailInput.value;
    const isValid = validate(email);

    if (isValid) {
      if (email === 'forbidden@gmail.com') {
        alert('Error: This email is not allowed.');
      } else {
        subscribeButton.disabled = true;
        unsubscribeButton.disabled = true;
        subscribeButton.style.opacity = '0.5';
        unsubscribeButton.style.opacity = '0.5';
        sendDataToServer(email);
      }
    } else {
      alert('Invalid email address!');
    }
  });

  function sendDataToServer(email) {
    fetch('http://localhost:3000/subscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: email }),
    })
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else if (response.status === 422) {
        return response.json().then((data) => {
          alert(`Error: ${data.message}`);
        });
      } else {
        alert('An error occurred while subscribing. Please try again later.');
        throw new Error('Network response was not ok.');
      }
    })
    .then(() => {
      // Subscription was successful, update UI accordingly
      const isSubscribed = localStorage.getItem("isSubscribed") === "true";
      if (isSubscribed) {
        emailInput.style.display = 'block';
        emailInput.value = '';
        subscribeButton.textContent = 'Subscribe';
        localStorage.removeItem("subscriptionEmail");
        localStorage.setItem("isSubscribed", "false");
      } else {
        emailInput.style.display = 'none';
        subscribeButton.textContent = 'Unsubscribe';
        localStorage.setItem("isSubscribed", "true");
      }
    })
    .catch((error) => {
      console.error('Error:', error);
    })
    .finally(() => {
      // Re-enable the buttons after the request is completed (successful or not)
      subscribeButton.disabled = false;
      unsubscribeButton.disabled = false;
      subscribeButton.style.opacity = '1';
      unsubscribeButton.style.opacity = '1';
    });
  }

  // Load community data on page load
  const communitySection = document.getElementById("community-section");
  if (communitySection) {
    fetch('http://localhost:3000/community')
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok.');
        }
        return response.json();
      })
      .then((data) => {
        const communityList = document.getElementById("community-list");
        data.forEach((person) => {
          const listItem = document.createElement("li");
          listItem.textContent = person.name;
          communityList.appendChild(listItem);
        });
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }

  // Unsubscribe button functionality
  unsubscribeButton.addEventListener('click', () => {
    unsubscribeButton.disabled = true;
    unsubscribeButton.style.opacity = '0.5';

    fetch('http://localhost:3000/unsubscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: localStorage.getItem("subscriptionEmail") }),
    })
    .then((response) => {
      if (response.ok) {
        emailInput.style.display = 'block';
        emailInput.value = '';
        subscribeButton.textContent = 'Subscribe';
        localStorage.removeItem("subscriptionEmail");
        localStorage.setItem("isSubscribed", "false");
      } else {
        alert('An error occurred while unsubscribing. Please try again later.');
        throw new Error('Network response was not ok.');
      }
    })
    .catch((error) => {
      console.error('Error:', error);
    })
    .finally(() => {
      unsubscribeButton.disabled = false;
      unsubscribeButton.style.opacity = '1';
    });
  });
});
